# schema-gitops

## Usage

The `changelog.sql` is what contains the schema statements for managing a DB via git.
In this case, a Merge Request will run a job that will run a liquibase status to make sure its readable/valid.
Upon merging there is an `Add_Data_Map` job that will scan for annotations to add a map to the Cyral Control Plane.
The format of that looks like

```sql
-- comment CYRAL_LABEL liquibase-ci LIQUIBASE_CCN public credit_cards credit_card_number
```

The `-- comment CYRAL_LABEL` is static and the follow are parameters, `data_repo` `label` `schema` `table` `field`

After that job has run successfully the `DB_Update` job will trigger and run the update against the database.

## Demo Usage Notes

### Starting Clean

1. Go to the [Pipeline list](https://gitlab.com/cyralinc/schema-gitops/-/pipelines) and select the first stage circle of any job with 3 stages, and hit the play button next to `Drop_Tables` and `Remove_All_Maps`
1. Make sure the changelog.sql only contains the following

```sql
-- liquibase formatted sql

-- changeset justin:create_table
CREATE TABLE credit_cards (
    id Integer Primary Key Generated Always as Identity,
    cardholder_name VARCHAR(50) NOT NULL,
    card_issue_date DATE NOT NULL
);
-- rollback DROP TABLE credit_cards;

-- changeset justin:inject_data
INSERT INTO credit_cards(cardholder_name, card_issue_date) VALUES ('Alice', '2023-01-01');
INSERT INTO credit_cards(cardholder_name, card_issue_date) VALUES ('Bob', '2023-01-02');
INSERT INTO credit_cards(cardholder_name, card_issue_date) VALUES ('Chuck', '2023-01-03');
INSERT INTO credit_cards(cardholder_name, card_issue_date) VALUES ('Dan', '2023-01-04');
INSERT INTO credit_cards(cardholder_name, card_issue_date) VALUES ('Erin', '2023-01-05');
-- rollback TRUNCATE TABLE credit_cards;
```

This will drop the liquibase state tables and current credit_cards table to allow rolling back the changelog.sql to the above

### Workflow

1. Load [changelog.sql](https://gitlab.com/cyralinc/schema-gitops/-/blob/main/changelog.sql) and select `Edit` the blue dropdown (Open in Web IDE and Open in Gitpod wont cut it)
1. Add the following to the end of the file

        -- changeset justin:add_ccn
        -- comment CYRAL_LABEL liquibase-ci LIQUIBASE_CCN public credit_cards credit_card_number
        ALTER TABLE credit_cards ADD COLUMN credit_card_number VARCHAR(19);
        UPDATE credit_cards SET credit_card_number = '4444-1234-1234-1234';
        -- rollback ALTER TABLE credit_cards DROP COLUMN credit_card_number;
1. Provider a name for the `Target Branch`
1. Commit Changes
1. Provider Merge Request Title
1. Create Merge Request
1. Review MR pipeline, which does a check and notifies of one changeset to be applied
1. Merge MR
1. Once merged the MR page will update to show the pipeline running on Main, click in to that
1. `DB_Deploy` job will run the change set and `Add_Data_Map` will add any new mappings. This job will log that it added the mapping
1. Manually run the `Test_Query` job to view the results.
1. Once complete follow the [Starting Clean](#starting-clean)
