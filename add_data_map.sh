#!/usr/bin/env bash
changelog="changelog.sql"

echo "Processing labels for $changelog..."

add_data_map() {
    if (($# != 5 )); then
        echo "Expecting 5 arguments, dataRepo label schema table field"
        echo "Got: $*"
        exit 1
    fi

    dataRepo="$1"
    label="$2"
    schema="$3"
    table="$4"
    field="$5"

    if ! command -v jq 2>&1 1>/dev/null; then
        echo "jq is required, please install it"
        exit 1
    fi

    if [ -z "$CYRAL_CONTROL_PLANE" ]; then
        echo "Missing Env Var: CYRAL_CONTROL_PLANE"
        exit 1
    fi

    if [ -z "$CYRAL_CLIENT_ID" ]; then
        echo "Missing Env Var: CYRAL_CLIENT_ID"
        exit 1
    fi

    if [ -z  "$CYRAL_CLIENT_SECRET" ]; then
        echo "Missing Env Var: CYRAL_CLIENT_SECRET"
        exit 1
    fi

    TOKEN=$(curl -s --request POST "https://$CYRAL_CONTROL_PLANE/v1/users/oidc/token" \
    -d grant_type=client_credentials \
    -d client_id="$CYRAL_CLIENT_ID" \
    -d client_secret="$CYRAL_CLIENT_SECRET" | jq -r .access_token)

    #validate repo
    repos=$(curl -s "https://$CYRAL_CONTROL_PLANE/v1/repos" -H "authorization: Bearer $TOKEN")
    if ! repo=$(echo "$repos" | jq ".repos[] | select(.repo.name == \"$dataRepo\")"); then
        echo "Unable to find repo $dataRepo"
        exit 1
    fi
    repoId=$(echo "$repo" | jq -r ".id")
    #get mappings
    dataMap=$(curl -s "https://$CYRAL_CONTROL_PLANE/v1/repos/${repoId}/datamap" -H "authorization: Bearer $TOKEN")
    attribute="${schema}.${table}.${field}"
    if ! index=$(echo "$dataMap" | jq -e ".labels.${label}.attributes | index( \"${attribute}\" )"); then
        echo "Creating $attribute for label $label on $dataRepo"
        output=$(curl -s --request PUT "https://$CYRAL_CONTROL_PLANE/v1/repos/${repoId}/datamap/labels/${label}/attributes/${attribute}" -H "authorization: Bearer $TOKEN")
        if [[ "$output" != "{}" ]]; then
            echo "Error creating datamap!"
            echo "$output"
            exit 1
        fi
    else
        echo "Label $label already has $attribute"
    fi
}


if  labels=$(grep "CYRAL_LABEL" $changelog); then
    while read -r line; do
        read -r -a args <<< "$(echo "$line" | rev | cut -d" "  -f1-5 | rev)"
        add_data_map "${args[0]}" "${args[1]}" "${args[2]}" "${args[3]}" "${args[4]}" 
    done < <(printf '%s\n' "$labels")
    echo "Labels successfully processed."
else
    echo "Change log $changelog has no annotations, exiting"
fi
