#!/usr/bin/env bash

dataRepo=$1

if ! command -v jq 2>&1 1>/dev/null; then
    echo "jq is required, please install it"
    exit 1
fi

if [ -z "$CYRAL_CONTROL_PLANE" ]; then
    echo "Missing Env Var: CYRAL_CONTROL_PLANE"
    exit 1
fi

if [ -z "$CYRAL_CLIENT_ID" ]; then
    echo "Missing Env Var: CYRAL_CLIENT_ID"
    exit 1
fi

if [ -z  "$CYRAL_CLIENT_SECRET" ]; then
    echo "Missing Env Var: CYRAL_CLIENT_SECRET"
    exit 1
fi

TOKEN=$(curl -s --request POST "https://$CYRAL_CONTROL_PLANE/v1/users/oidc/token" \
-d grant_type=client_credentials \
-d client_id="$CYRAL_CLIENT_ID" \
-d client_secret="$CYRAL_CLIENT_SECRET" | jq -r .access_token)

repos=$(curl -s "https://$CYRAL_CONTROL_PLANE/v1/repos" -H "authorization: Bearer $TOKEN")
if ! repo=$(echo "$repos" | jq ".repos[] | select(.repo.name == \"$dataRepo\")"); then
    echo "Unable to find repo $dataRepo"
    exit 1
fi
repoId=$(echo "$repo" | jq -r ".id")
dataMapDelete=$(curl -s --request DELETE "https://$CYRAL_CONTROL_PLANE/v1/repos/${repoId}/datamap" -H "authorization: Bearer $TOKEN")

echo "Delete response: $dataMapDelete"